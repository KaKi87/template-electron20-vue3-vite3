git clone --recurse-submodules https://git.kaki87.net/KaKi87/template-electron20-vue3-vite3.git "$1"
cd "$1"
rm -r .git
git init
git add .
git commit -m ":tada: Initial commit"
yarn install
cp config.example.js config.js
bash
exit