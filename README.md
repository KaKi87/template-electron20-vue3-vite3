# `template-electron20-vue3-vite3`

Opinionated [Electron](https://github.com/electron/electron) 20.x + [Vue](https://github.com/vuejs/vue) 3.x + [Vite](https://github.com/vitejs/vite) 3.x template

![](./src-electron/assets/icon.png)

## Specs

- `process.env.NODE_ENV` available from renderer script
- Configurable
  - title & menu bar visibility
  - icon
  - CORS
  - spellcheck
- Developer-friendly
  - Automatically opening DevTools
  - Default detached DevTools mode
  - Includes [Vue DevTools](https://github.com/vuejs/devtools)
  - Includes [destyle.css](https://github.com/nicolas-cusan/destyle.css)
- Cross-platform
  - `start` & `build` scripts
  - builds (AppImage for Linux, NSIS for Windows, DMG for Mac)
- Fully dark app & DevTools
- Shows window once ready

## Installation

### Automated

`curl -s https://git.kaki87.net/KaKi87/template-electron20-vue3-vite3/raw/branch/master/clone.sh | bash -s hello-world`

### Manual

Clone existing repo & create new repo :

```bash
git clone --recurse-submodules https://git.kaki87.net/KaKi87/template-electron20-vue3-vite3.git hello-world
cd hello-world
rm -r .git
git init
git add .
git commit -m ":tada: Initial commit"
```

Install dependencies : `yarn install`

Create configuration file : `cp config.example.js config.js`

## Usage

Start development server using `yarn start`
(outputs temporary files in `dist`)

Create production build using `yarn build`
(outputs production file in `build`)

Develop everything inside `src-electron` & `src-vue`

---

![](https://i.goopics.net/3l6wo9.png)